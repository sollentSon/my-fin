export enum Currency {
    USDT = 'USDT',
    USD = 'USD',
    RUB = 'RUB',
    BTC = 'BTC',
    ETH = 'ETH'
}

export const CurrenciesArray = Object.values(Currency)

export const Symbols = {
    [Currency.USDT]: '/currencies/usdt.svg',
    [Currency.USD]: '/currencies/usd.svg',
    [Currency.RUB]: '/currencies/rub.svg',
    [Currency.BTC]: '/currencies/btc.svg',
    [Currency.ETH]: '/currencies/eth.svg'
}
