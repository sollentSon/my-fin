import { useStorage } from '@vueuse/core'
import { v4 as uuid } from 'uuid'
import { InvestmentTool } from '../types/InvestmentTool.ts'

export const useInvestmentTools = () => {
    const tools = useStorage<InvestmentTool[]>('tools', [])

    const setTool = (tool: Omit<InvestmentTool, 'id'>) => {
        tools.value.push({ ...tool, id: uuid() })
    }

    return {
        tools,
        setTool
    }
}
