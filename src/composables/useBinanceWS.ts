import { Ref } from 'vue'
import { isNaN } from 'lodash'
import { useThrottleFn } from '@vueuse/core'

export const cryptoPairs = ['btcusdt', 'ethusdt', 'ltcusdt', 'xrpusdt', 'linkusdt', 'wavesusdt', 'algousdt']

interface IWSResponse {
    e: string
    E: number
    s: string
    t: number
    p: string
    q: string
    b: number
    a: number
    T: number
    m: boolean
    M: boolean
}

export type PriceClass = 'success' | 'danger' | 'primary'

interface ICrypto {
    id: number
    name: string
    price: null | string
    className: PriceClass
    socket: WebSocket
}

export const useBinanceWS = () => {
    const isSocketOpen = ref(false)

    const socket = (name: string) => new WebSocket(`wss://stream.binance.com:9443/ws/${name}@trade`)

    const cryptoGenerator = function* (cryptoPairs: string[]): Generator<ICrypto> {
        for (let i = 0; i < cryptoPairs.length; i++)
            yield { id: i, name: cryptoPairs[i], price: null, className: 'primary', socket: socket(cryptoPairs[i]) }
    }

    const determineClass = (newValue: string, oldValue: string | null): PriceClass => {
        const [newInner, oldInner] = [Number(newValue), Number(oldValue)]

        if (oldValue && !isNaN(newInner) && !isNaN(oldInner)) {
            if (newValue > oldValue) return 'success'
            if (newValue < oldValue) return 'danger'
        }

        return 'primary'
    }

    const cryptoSocketArray: Ref<ICrypto[]> = ref([...cryptoGenerator(cryptoPairs)])

    const startSockets = () => {
        cryptoSocketArray.value.forEach((item) => {
            item.socket.onopen = () => (isSocketOpen.value = true)
            item.socket.onclose = () => (isSocketOpen.value = false)

            const onMessage = useThrottleFn((message: MessageEvent) => {
                const data = JSON.parse(message.data) as IWSResponse
                const innerValuePrice = Number(data.p)

                if (!isNaN(innerValuePrice)) {
                    const currentValue = innerValuePrice.toFixed(['xrp', 'link', 'algo'].includes(item.name.replace('usdt', '')) ? 4 : 2)

                    item.className = determineClass(currentValue, item.price)
                    item.price = currentValue
                }
            }, 1250)

            item.socket.onmessage = (message: MessageEvent) => onMessage(message)
        })
    }

    const stopSockets = () => cryptoSocketArray.value.forEach((item) => item.socket?.close(1000))

    onMounted(() => {
        startSockets()
    })
    onBeforeUnmount(() => {
        stopSockets()
    })

    return {
        isSocketOpen,
        cryptoSocketArray
    }
}
