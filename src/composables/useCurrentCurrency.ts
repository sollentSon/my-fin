import { useStorage } from '@vueuse/core'
import { Currency } from '../constants/currencies.ts'

export const useCurrentCurrency = () => {
    const currency = useStorage<Currency>('currency', Currency.USDT)

    const basedCurrencies = [Currency.USDT, Currency.USD, Currency.RUB, Currency.BTC]

    return {
        currency,
        basedCurrencies
    }
}
