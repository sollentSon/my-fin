import { Currency } from '../constants/currencies.ts'

export interface InvestmentTool {
    investment: string | number
    currency: Currency
    name: string
    id: string
}
